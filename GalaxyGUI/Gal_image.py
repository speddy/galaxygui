"""Program is a basic Tkinter GUI used to illustrate some of the methods used in the project (Moment of Inertia Calculation, and R90/R50)
Made in Python 2.7 """
from __future__ import division
import numpy as np
try:
    # for Python2
    import Tkinter as tk
except ImportError:
    # for Python3
    import tkinter as tk
import matplotlib.pyplot as plt
import matplotlib.figure as mplfig
import matplotlib.backends.backend_tkagg as tkagg
from astropy.io import fits
import os,sys
import matplotlib as mpl
from functools import partial

label_size =25
mpl.rcParams['xtick.labelsize'] = label_size
mpl.rcParams['ytick.labelsize'] = label_size 

def get_gals():
    """Returns two lists of galaxies in the folder """
    items = os.listdir(".\\gals")
    edge = [w for w in items if w.startswith('galedge_')]
    face = [w for w in items if w.startswith('galface_')]
    return sorted(edge),sorted(face)
    
def get_galaxyImageEdge(GalNum,frameNum,minimum):
    """Opens a FITS galaxy file for an edge-on galaxy and returns a log image,
    a pixel array and a max pixel value"""
    hdulistedge = fits.open(".\\gals\\"+edgeGals[GalNum])
    dataedge = hdulistedge[0].data
    Bandedge = dataedge[frameNum]/(4.8532896**2) # a unit conversion
    BandedgeZero = (Bandedge <= minimum)
    Bandedge[BandedgeZero]=minimum
    loggerEdge=np.log10(Bandedge)
    maxVal=np.max(loggerEdge)
    return loggerEdge,maxVal,Bandedge
    
def get_galaxyImageFace(GalNum,frameNum,minimum):
    """Opens a FITS galaxy file for an face-on galaxy and returns a log image,
    a pixel array and a max pixel value"""
    hdulistface = fits.open(".\\gals\\"+faceGals[GalNum])
    dataface = hdulistface[0].data
    Bandface = dataface[frameNum]/(4.8532896**2) # a unit conversion
    BandfaceZero = (Bandface <= minimum)
    Bandface[BandfaceZero]=minimum
    loggerFace=np.log10(Bandface)
    maxVal=np.max(loggerFace)
    # returns log(image minus zero values), maxValue of image, raw image array
    return loggerFace,maxVal,Bandface
    
def returnLight(Band,rad):
    ''' returns the light enclosed within a certain radii, and the number of pixels used. '''
    acondition = (r<=rad)
    oners = np.ones((256,256))
    return np.sum(Band[acondition]),np.sum(oners[acondition])

def find_R(Band,total,ratio):
    '''Takes the 2D image, the total light and a ratio x, returns the light within x '''
    # lets do this by the bisection method, a,b inital = max radius and the min radius
    a,b,error,val,q= 128,0,1,0.0001,0
    while (error >val):
        q+=1
        c= (a+b)/2
        cLight = returnLight(Band,c)[0]
        if (cLight > ratio*total):
            a=c
        elif (cLight < ratio*total):
            b=c
        error = abs(cLight-ratio*total)/total
        if q>100:
            break
    return c,error

def mOinertia(Band):
    """Finds the moment of Inertia ratio plus eigenvalues"""
    I11,I22,I12 = 0,0,0
    I11 = np.sum(Band*xx**2)
    I22 = np.sum(Band*yy**2)
    I12 = np.sum(Band*xx*yy)
    I1 = np.matrix([[I11,I12],[I12,I22]])
    eigens = np.linalg.eig(I1) # diagonalise
    eigs,vectors = eigens[0],eigens[1] # get the vectors and eigenvalues out
    return  (eigs[0]/eigs[1]),vectors[:,0],vectors[:,1]


class App(object):
    def __init__(self, master,galaxyNum=0,frame=3,cutOffMin=1e-6):
        """Initialises the GUI frame and loads the first galaxy """
        self.master = master
        self.galaxyNum=galaxyNum
        self.toggler=False # False if face-on image currently displayed
        self.frame = frame # a default frame to image
        self.cutOffMin=cutOffMin
         # a default SB limit for the Moment of Inertia calculation
        loggerFace,maxVal,RawImage = get_galaxyImageFace(self.galaxyNum,self.frame,self.cutOffMin) 
        self.RawImage = RawImage
        
        # **** Menu ****
        self.menubar= tk.Menu(self.master)
        self.FileMenu = tk.Menu(self.menubar,tearoff=0)
        self.menubar.add_cascade(label="File",menu=self.FileMenu)
        self.FileMenu.add_command(label="Save",command=self.save)
        self.OptionsMenu =tk.Menu(self.menubar,tearoff=0)
        self.FrameMenu=tk.Menu(self.OptionsMenu,tearoff=0)
        self.cutOffMenu=tk.Menu(self.OptionsMenu,tearoff=0)
        self.menubar.add_cascade(label="Options",menu=self.OptionsMenu)
        self.OptionsMenu.add_cascade(label="Frame",menu=self.FrameMenu)
        self.OptionsMenu.add_cascade(label="Pixel Cut-off Min",menu=self.cutOffMenu)
        self.cutOffMenu.add_command(label="1e-5",command=partial(self.set_min,1e-5))        
        self.cutOffMenu.add_command(label="1e-6",command=partial(self.set_min,1e-6))
        self.cutOffMenu.add_command(label="1e-7",command=partial(self.set_min,1e-7))
        self.FrameMenu.add_command(label="u",command=partial(self.set_frame,0))
        self.FrameMenu.add_command(label="g",command=partial(self.set_frame,1))
        self.FrameMenu.add_command(label="r",command=partial(self.set_frame,2))
        self.FrameMenu.add_command(label="i",command=partial(self.set_frame,3))
        self.FrameMenu.add_command(label="z",command=partial(self.set_frame,4))
        self.master.config(menu=self.menubar)

        # **** Toolbar ****
        self.toolbar = tk.Frame(self.master)
        self.b1=tk.Button(self.toolbar,text="Next Galaxy", command=self.update_galaxy)
        self.b1.pack(side=tk.LEFT,padx=2,pady=2)
        self.b2 = tk.Button(self.toolbar,text="Toggle View",command = self.switch_view)
        self.b2.pack(side=tk.LEFT,padx=2,pady=2)
        self.toolbar.pack(side=tk.TOP,fill=tk.X)
        self.b3=tk.Button(self.toolbar,text="Mo_Inertia",command=self.Moment_Of_Inertia)
        self.b3.pack(side=tk.LEFT,padx=2,pady=2)
        self.b4=tk.Button(self.toolbar,text="R90/R50",command=self.R90R50)
        self.b4.pack(side=tk.LEFT,padx=2,pady=2)
        
        # **** Canvas for matplotlib figure ******
        self.fig = mplfig.Figure(figsize=(8, 6))
        self.ax1 = self.fig.add_axes([0.1, 0.1, 0.75, 0.75])
        self.canvas = tkagg.FigureCanvasTkAgg(self.fig, master=self.master)
        self.ax1.grid(False)
        # **** Initialise two circles for R90,R50 and an ellipse for the moment of Inertia *****
        self.circleR90 = plt.Circle((128,128),0,color='black', fill=False)
        self.ax1.add_artist(self.circleR90)
        self.circleR50 = plt.Circle((128,128),0,color='black',fill=False)
        self.ax1.add_artist(self.circleR50)
        self.MoIellipse = mpl.patches.Ellipse(xy=(128,128), width=200, height=50,
                                        angle=0, edgecolor='black',fc ='None')
        self.ax1.add_artist(self.MoIellipse)
        # Turn them all off for now 
        self.circleR90.set_visible(False)
        self.circleR50.set_visible(False)
        self.MoIellipse.set_visible(False)
        self.image= self.ax1.imshow(loggerFace,cmap = 'magma',extent = [0,256,0,256],
                                                    origin = 'lower')
        self.cb =self.fig.colorbar(self.image)
        self.cb.set_label(r'$\rm{log_{10}}(\rm{SB}~{[mJy~ arcsec^{-2}]})$',
                                                    fontsize = label_size,labelpad = 15)
        self.canvas.get_tk_widget().pack()
        self.canvas.draw()
        
        # *** Status bar at the bottom of the frame ***
        self.status = tk.Label(self.master,text="Galaxy {}, Max: {} Min: {}, Frame={}".format(
                        faceGals[self.galaxyNum],round(maxVal,2),np.log10(self.cutOffMin),frames[self.frame]),
                        bd=1,relief=tk.SUNKEN,anchor=tk.W) 
        self.status.pack(side=tk.BOTTOM,fill=tk.X)
    
    
    def save(self):
        """Save the Matplotlib figure """
        self.fig.savefig('{}.png'.format(self.galaxyNum),bbox_inches='tight')
    
    def set_min(self,minimum):
        """Changes the cut-off point on the frames (0.0 values in the image have to be given a small value say 1e-6 in order to take Log """
        self.circleR90.set_visible(False)
        self.circleR50.set_visible(False)
        self.MoIellipse.set_visible(False)
        self.cutOffMin=minimum
        loggerFace,maxVal,RawImage= get_galaxyImageFace(self.galaxyNum,self.frame,self.cutOffMin)
        self.RawImage=RawImage # used for the R90/R50 and MoI calculations
        self.status["text"]= "Galaxy {}, Max: {} Min: {}, Frame={}".format(
                            faceGals[self.galaxyNum],round(maxVal,2),np.log10(self.cutOffMin),frames[self.frame]) 
        self.image.set_data(loggerFace)
        self.image.autoscale()
        self.cb.update_normal(self.image)
        self.canvas.draw()       
                              
    def set_frame(self,num):
        """Changes the frame and refreshes image. Galaxies are viewed in different filter frames (rest frames),
        u= shortest wavelength down to z =longest wavelength """
        self.circleR90.set_visible(False)
        self.circleR50.set_visible(False)
        self.MoIellipse.set_visible(False)
        self.frame=num
        loggerFace,maxVal,RawImage = get_galaxyImageFace(self.galaxyNum,self.frame,self.cutOffMin)
        self.RawImage=RawImage
        self.status["text"]= "Galaxy {}, Max: {} Min: {}, Frame={}".format(
                            faceGals[self.galaxyNum],round(maxVal,2),np.log10(self.cutOffMin),frames[self.frame])
        self.image.set_data(loggerFace)
        self.image.autoscale()
        self.cb.update_normal(self.image)
        self.canvas.draw()
        
    def update_galaxy(self):
        """Increase the click count update image data to load a new galaxy image (in face on view) """
        self.circleR90.set_visible(False)
        self.circleR50.set_visible(False)
        self.MoIellipse.set_visible(False)
        self.galaxyNum += 1 # we have had next galaxy Command so update the galaxy Num
        if self.galaxyNum == len(edgeGals): # we need to complete the loop
            self.galaxyNum=0
        loggerFace,maxVal,RawImage = get_galaxyImageFace(self.galaxyNum,self.frame,self.cutOffMin)
        self.RawImage=RawImage
        self.status["text"]= "Galaxy {}, Max: {} Min: {}, Frame={}".format(
                            faceGals[self.galaxyNum],round(maxVal,2),np.log10(self.cutOffMin),frames[self.frame])
        self.image.set_data(loggerFace)
        self.image.autoscale()
        self.cb.update_normal(self.image)
        self.canvas.draw()
        self.toggler=False
    
    def switch_view(self):
        """Linked to Toggle View, toggles between edge-on and face-on views """
        self.circleR90.set_visible(False)
        self.circleR50.set_visible(False)
        self.MoIellipse.set_visible(False)

        if self.toggler == False:
            loggerEdge,maxVal,RawImage=get_galaxyImageEdge(self.galaxyNum,self.frame,self.cutOffMin)
            self.RawImage = RawImage
            self.status["text"]= "Galaxy {}, Max: {} Min: {}, Frame={}".format(
                            edgeGals[self.galaxyNum],round(maxVal,2),np.log10(self.cutOffMin),frames[self.frame])
            self.image.set_data(loggerEdge)
            self.image.autoscale()
            self.cb.update_normal(self.image)            
        else:
            loggerFace,maxVal,RawImage = get_galaxyImageFace(self.galaxyNum,self.frame,self.cutOffMin)
            self.RawImage=RawImage
            self.status["text"]= "Galaxy {}, Max: {} Min: {}, Frame={}".format(
                            faceGals[self.galaxyNum],round(maxVal,2),np.log10(self.cutOffMin),frames[self.frame])
            self.image.set_data(loggerFace)
            self.image.autoscale()
            self.cb.update_normal(self.image)               
        self.canvas.draw()
        self.toggler^=True # toggle

    def Moment_Of_Inertia(self):
        """Plot the moment of Inertia ellipse, with the ratio factor in the status bar """
        
        self.circleR90.set_visible(False)
        self.circleR50.set_visible(False)
        MoIcall = mOinertia(self.RawImage)
        self.ratio=MoIcall[0] #  get the axes ratio
        self.height=1
        Eigenvector = MoIcall[1]
        self.EllipseAngle =  np.degrees(np.arctanh((Eigenvector[1]/Eigenvector[0])))
        # remove the exising ellipse patch and add the new one
        self.MoIellipse.remove()
        self.MoIellipse = mpl.patches.Ellipse(xy=(128,128), width=self.ratio*20, 
                                            height=self.height*20, angle=self.EllipseAngle,
                                            edgecolor='black',fc ='None')
        self.ax1.add_artist(self.MoIellipse)
        self.MoIellipse.set_visible(True)
        self.status["text"]= "Axes Ratio:{}, Ellipse Tilt: {} degrees (anticlockwise) ".format(
                                    str(round(self.ratio,2)),str(round(self.EllipseAngle,2)))
        self.canvas.draw()
        
    def R90R50(self):
        """Plot contours representing R90 and R50, then update status with a value """
        self.MoIellipse.set_visible(False)
        totalLight=returnLight(self.RawImage,256)[0] # get the total light in the image
        R50,R50error = find_R(self.RawImage,totalLight,0.5)
        R90,R90error = find_R(self.RawImage,totalLight,0.9)
        R90R50=R90/R50
        self.circleR90.set_radius(R90)
        self.circleR50.set_radius(R50)
        self.circleR90.set_visible(True)
        self.circleR50.set_visible(True)
        self.canvas.draw()
        self.status["text"]= "R90/R50: " +str(round(R90R50,2))


edgeGals,faceGals=get_gals()
xx,yy = np.meshgrid(np.arange(-128+0.5,128+0.5),np.arange(-128+0.5,128+0.5)) 
# create meshgrids of x,y coordinates
r = np.sqrt(xx**2 + yy**2) # pixel distance from the centre
frames=["u","g","r","i","z"]
root = tk.Tk()
root.title("Eagle Galaxy GUI")
app = App(root)
tk.mainloop()
