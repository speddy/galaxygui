'''Let us calculate the moment of inertia for the images and then use that to try and see some bi-modality ''' 
from __future__ import division
from astropy.io import fits
import matplotlib.pyplot as plt
import numpy as np
import sys
import math
import time
from matplotlib.patches import Ellipse
import matplotlib as mpl
# some plotting code,setup
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
label_size =45
mpl.rcParams['xtick.labelsize'] = label_size
mpl.rcParams['ytick.labelsize'] = label_size 
mpl.rcParams['figure.figsize'] = 15, 11.5

SBLimit = 0.0005
galIDlist = ['17710821','8136011','8414201','8375961']
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# i want to find the radius at which the surface brightness falls below a certain limit
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Just define some of the properties soecific to this Run
framenumber = 3
frame = ['u','g','r','i','z']
factor =1
xx,yy = np.meshgrid(np.arange(-(128*factor)+0.5,(128*factor)+0.5),np.arange(-(128*factor)+0.5,(128*factor)+0.5)) # create meshgrids of x,y coordinates
r = np.sqrt(xx**2 + yy**2) # pixel distance from the centre
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def returnLight(Band,rad):
    ''' returns the light enclosed within a certain radii, and the number of pixels used '''
    acondition = (r<=rad)
    oners = np.ones((256,256))
    return np.sum(Band[acondition]),np.sum(oners[acondition])


def find_petrosian(Band,SBlim):
    # the petrosian radius has to be between these two radii.
    guess,guess2 = 256,1
    error,val=1,0.001
    q=0
    while (error>val):
        q +=1
        c = (guess+guess2)/2
        Light = returnLight(Band,c)
        Lightminus =returnLight(Band,(c-1))
        ShellLight1 = (Light[0]-Lightminus[0])/(Light[1]-Lightminus[1])
        if (ShellLight1< SBlim):
            # then need to go closer in
            guess =c
        elif (ShellLight1>SBlim):
            guess2 =c
        if q>100:    
            break
        error = abs(ShellLight1 -SBlim)/SBlim
        
    return c,ShellLight1
    
        


#choose the galaxy
galID = galIDlist[0]

#hdulistFACE=fits.open('galface_%s.fits'%galID)
hdulistEDGE = fits.open('galedge_%s.fits'%galID)
#dataFACE=hdulistFACE[0].data
dataEDGE = hdulistEDGE[0].data
#BandFACE= dataFACE[framenumber]
BandEDGE = dataEDGE[framenumber]
#BandFACE=BandFACE/((4.8532896**2)) 
BandEDGE =  BandEDGE/((4.8532896**2)) # correct to get units into SB

def findSBradius(Band,SBLimit):
    '''takes the surface brightness aperture limit and finds the limiting radius '''
    limitradius = find_petrosian(Band,SBLimit)
    return limitradius

def mOinertia(Band,limitradius):
    """Finds the moment of Inertia ratio plus eigenvalues"""
    I11,I22,I12 = 0,0,0
    aperture = (r < limitradius) # only use pixels within the aperture
    newBand= Band[aperture]
    newXX = xx[aperture]
    newYY =yy[aperture]
    I11 = np.sum(newBand*newXX**2)
    I22 = np.sum(newBand*newYY**2)
    I12 = np.sum(newBand*newXX*newYY)
    I1 = np.matrix([[I11,I12],[I12,I22]])
    eigens = np.linalg.eig(I1) # diagonalise
    eigs,vectors = eigens[0],eigens[1] # get the vectors and eigenvalues out
    return  (eigs[0]/eigs[1]),vectors[:,0],vectors[:,1]
    
# MOMENT OF INERTIA GRAPH CODE

#limitradius = findSBradius(BandFACE)[0]

#call= mOinertia(BandFACE,limitradius)
#widthFACE = call[0]

height =1
#face1 = call[1]
#angleFACE  =np.degrees(np.arctanh((face1[1]/face1[0])))


limitradiusEdge = findSBradius(BandEDGE,SBLimit)[0]
print "lim",limitradiusEdge
callEdge =mOinertia(BandEDGE,limitradiusEdge)
widthEDGE = callEdge[0]
print "ratio",widthEDGE
edge1 = callEdge[1]
angleEDGE = np.degrees(np.arctanh((edge1[1]/edge1[0])))
print angleEDGE

'''
fig1,ax1 = plt.subplots()
image1 = plt.imshow((np.log10(BandFACE)),cmap= 'hot',extent = [0,256,0,256],origin = 'lower')
plt.colorbar(image1)
ellipse1 = Ellipse(xy=(128,128), width=widthFACE*60, height=height*30, angle=angleFACE, edgecolor='black',fc ='None', lw=5)
circleLimit1 = plt.Circle((256/2,256/2),limitradius,color = 'blue',fill = False)
ax1 = fig1.gca()
ax1.add_artist(ellipse1)
ax1.add_artist(circleLimit1)
ax1.spines['top'].set_visible(False)
ax1.spines['right'].set_visible(False)
ax1.get_xaxis().tick_bottom()
ax1.get_yaxis().tick_left()
plt.xlabel(r'X')
plt.ylabel(r'Y')
plt.close()
# CHANGE ANGLES TO DEGREES!!!
'''

condition =BandEDGE < 0.0000001
BandEDGE[condition] = 0.0000001
loggerEDGE = np.log10(BandEDGE)


fig2,ax2 = plt.subplots()
image2 = plt.imshow((loggerEDGE),vmin = -6,vmax=-1,cmap= 'magma',extent = [0,256,0,256],origin = 'lower')
#image2 = plt.imshow(BandEDGE,cmap= 'magma',extent = [0,256,0,256],origin = 'lower')
ellipse2 = Ellipse(xy=(128,128), width=widthEDGE*25, height=height*25, angle=angleEDGE, edgecolor='black',fc ='None', lw=5)
circleLimit2 = plt.Circle((256/2,256/2),limitradiusEdge,color = 'white',fill = False,lw = 10,ls='--')
ax2 = fig2.gca()
ax2.add_artist(ellipse2)
ax2.add_artist(circleLimit2)
ax2.spines['top'].set_visible(False)
ax2.spines['right'].set_visible(False)
ax2.get_xaxis().tick_bottom()
ax2.get_yaxis().tick_left()
plt.xlabel(r'X pixel',fontsize = label_size)
plt.ylabel(r'Y pixel',fontsize = label_size)
cb =plt.colorbar(image2)
cb.set_label(r'$\rm{log_{10}}(\rm{SB}~[mJy~ arcsec^{-2}])$',fontsize = label_size,labelpad = 15)
plt.tight_layout()
plt.show()


